const Palindromes = require('../model/Palindromes');

const express = require('express');
const router = express.Router();

/* GET home page. */
router.get('/', (req, res) => {
  res.json({
    results: Palindromes.fetch(req.query.cursor)
  });
});

module.exports = router;
