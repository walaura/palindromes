let store = [];

const uuid = require('uuid/v1');
const isPalindrome = require('../util/isPalindrome.js');
const cleanString = require('../util/cleanString.js');

module.exports = class Palindromes {

  static add(palindrome) {

    return new Promise((resolve,reject)=>{

      /*
			is it anything at all?
			*/
      if(!palindrome || palindrome.length < 1) {
        return reject(new Error('ERR_UNDEFINED_ARGUMENT'));
      }

      /*
			is it a palindrome?
			*/
      if(!isPalindrome(palindrome)) {
        return reject(new Error('ERR_NOT_A_PALINDROME'));
      }

      /*
			if it's a dupe delete the original &
			promote the new one to the top
			*/
      store = store.filter(item => (
        cleanString(item.palindrome) !== cleanString(palindrome)
      ));

      /*
			and push it
			*/
      store.push({
        cursor: uuid(),
        palindrome: palindrome,
        date: Date.now()
      });
      resolve();

    });

  }

  static reset() {
    store = [];
  }

  static fetch(cursor=undefined,rpp=10) {

    /*
		clean up the store to wipe old (10 m) palindromes, ideally
		housekeeping such as this would be on a storage layer
		*/
    store = store.filter(item => (
      item.date > (Date.now() - 600000)
    ));

    let returnable = [...store].reverse();
    let returnableCursor = undefined;
    let cutoff = 0;

    /*
    paginate only on a full list
    */
    if(returnable.length > 0) {

      /*
      find the next element closest
      to the last given uuid, this ensures that
      if the latest given uuid was deleted since then
      the result is still accurate
      */
      if(cursor) {
        cutoff = returnable.findIndex(element=>(
          parseInt(cursor,16) >= parseInt(element.cursor,16)
        ));
        if(cutoff === -1) {
          throw new Error('ERR_UNKNOWN_CURSOR');
        }
        else {
          cutoff = cutoff+1;
        }
      }

      returnable = returnable.slice(cutoff,rpp+cutoff);

      /*
      only return a pagination cursor if there's more to paginate
      */
      returnableCursor = returnable[returnable.length-1].cursor;
      if(returnableCursor === store[0].cursor) {
        returnableCursor = undefined;
      }

    }

    return {
      list: returnable,
      cursor: returnableCursor,
      total: store.length,
    };

  }

};
