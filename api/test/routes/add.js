const request = require('supertest');
const app = require('./../../app.js');
const chai = require('chai');

describe('POST /add', function() {
  it('should respond with json', function() {
    return request(app)
      .post('/add')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(500);
  });
  it('should connect with the model', function() {
    return request(app)
      .post('/add')
      .send({
        palindrome: 'aba'
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        chai.expect(response.body.status).to.equal(true);
      });
  });
});
