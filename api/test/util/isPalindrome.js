const chai = require('chai');
const isPalindrome = require('./../../util/isPalindrome.js');

describe('util/isPalindrome', function() {

  it('should fail if empty',()=>{

    chai.expect(
      isPalindrome.bind(this,'')
    ).to.throw('ERR_UNDEFINED_ARGUMENT');

  });

  it('should detect a palindrome',()=>{

    chai.expect(
      isPalindrome('A man, a plan, a cat, a ham, a yak, a yam, a hat, a canal-Panama!')
    ).to.equal(true);

  });

  it('should detect a wrong palindrome',()=>{

    chai.expect(
      isPalindrome('A man, a nap, a cat, a ham, a yak, a yam, a hat, a canal-Panama!')
    ).to.equal(false);

  });


});
