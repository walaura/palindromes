import React from 'react';
import ReactDOM from 'react-dom';
import Palindrome from './../../src/module/Palindrome';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Palindrome>Content</Palindrome>, div);
});
