import {add, subscribe} from './../../src/store/messages';

it('adds, then deletes, a message', (done) => {

  let tick = 0;

  subscribe(msgs => {
    if(tick === 0) {
      expect(msgs.length).toEqual(1);
      tick++;
    }
    else if(tick === 1) {
      expect(msgs.length).toEqual(0);
      done();
    }
  });

  add('test');

});
