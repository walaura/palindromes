import React from 'react';
import ReactDOM from 'react-dom';
import { mount } from 'enzyme';
import App from './../src/App';
import {add} from './../src/store/messages';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
});

it('contains the proper state', () => {
  const $wrapper = mount(<App />);

  expect(Object.keys($wrapper.state())).toContain('messages');
  expect(Object.keys($wrapper.state())).toContain('palindromes');
  expect(Object.keys($wrapper.state())).toContain('cursor');
});

it('is linked to messages', (done) => {
  const $wrapper = mount(<App />);
  add('test');
  setTimeout(()=>{
    expect($wrapper.state('messages')[0].text).toEqual('test');
    done();
  },500);
});
