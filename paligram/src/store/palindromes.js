/* improvised redux-like store */
import {API_ENDPOINT} from './../config';
import {add as messageAdd} from './messages';

let palindromes = [];
let cursor = undefined;
let subscriptions = [];

const notify = () => {
  subscriptions.map(notify => notify({
    palindromes: palindromes,
    cursor: cursor,
  }));
};

export const fetchFromScratch = () => {
  return fetch(API_ENDPOINT+'/list')
    .then(response => response.json())
    .then(data => {
      cursor = data.results.cursor;
      palindromes = data.results.list;
      notify();
    })
    .catch(error => {
      messageAdd(`Something went wrong (${error}) `);
    });
};

export const fetchFromCursor = () => {

  if(cursor === undefined) {
    return Promise.reject(new Error('ERR_UNDEFINED_CURSOR'));
  }

  return fetch(API_ENDPOINT+`/list?cursor=${cursor}`)
    .then(response => response.json())
    .then(data => {
      cursor = data.results.cursor;
      palindromes = [...palindromes, ...data.results.list];
      notify();
    })
    .catch(error => {
      messageAdd(`Something went wrong (${error}) `);
    });
};

export const subscribe = callback => {
  subscriptions.push(callback);
};
