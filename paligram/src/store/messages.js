const uuid = require('uuid/v1');

/* improvised redux-like store */
let messages = [];
let subscriptions = [];

export const add = message => {
  messages.push({
    uuid: uuid(),
    text: message
  });
  subscriptions.map(notify => notify(messages));
  setTimeout(()=>{
    messages.pop();
    subscriptions.map(notify => notify(messages));
  },4000);
};

export const subscribe = callback => {
  subscriptions.push(callback);
};
