/*eslint no-console:0 */

import React, { Component } from 'react';
import styles from './index.css';
import {API_ENDPOINT} from './../../config';
import {add as messageAdd} from './../../store/messages';
import {fetchFromScratch as palindromeFetchFromScratch} from './../../store/palindromes';

class Palindrome extends Component {

  constructor(props) {
    super(props);

    this.state = {
      palindromeToSubmit: '',
      status: '',
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({palindromeToSubmit: event.target.value});
  }

  handleSubmit(ev) {
    ev.preventDefault();

    const palindromeToSubmit = this.state.palindromeToSubmit;

    this.setState({
      palindromeToSubmit: '',
      status: 'POSTING'
    });

    fetch(API_ENDPOINT+'/add',{
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        palindrome: palindromeToSubmit
      })
    })
      .then(response => response.json())
      .then(data => {
        if(!data.error && !data.status) return Promise.reject('ERR_NOT_A_PALINDROME');
        if(!data.status) return Promise.reject(data.error);
        palindromeFetchFromScratch();
      })
      .catch(error=>{
        console.error(error);
        this.setState({
          palindromeToSubmit: palindromeToSubmit,
        });
        if(error === 'ERR_NOT_A_PALINDROME') {
          messageAdd('That\'s not a palindrome!');
        }
        else if(error === 'ERR_UNDEFINED_ARGUMENT') {
          messageAdd('Write something!');
        }
        else {
          messageAdd(`Something went wrong (${error}) `);
        }
      })
      .then(()=>{
        this.setState({status: ''});
      });

  }

  render() {

    return (
      <form className={styles.composer} onSubmit={this.handleSubmit} >
        <header className={styles.input}>
          <textarea placeholder='Add yours' name='palindrome' value={this.state.palindromeToSubmit} onChange={this.handleChange} ></textarea>
          <footer>
            {this.state.status === 'POSTING'
              ?
              (<span>on it</span>)
              :
              <button type='submit'>Post</button>
            }

          </footer>
        </header>
      </form>
    );
  }
}

export default Palindrome;
