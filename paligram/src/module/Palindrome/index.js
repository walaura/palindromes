import React, { Component } from 'react';
import styles from './index.css';
import PropTypes from 'prop-types';


class Palindrome extends Component {

  static get propTypes() {
    return {
      date: PropTypes.string,
      children: PropTypes.node.required
    };
  }

  render() {
    return (
      <div className={styles.palindrome}>
        <h3>{this.props.children}</h3>
        {this.props.date &&
          <h4>{new Date(this.props.date).toLocaleString('en-US')}</h4>
        }
      </div>
    );
  }
}

export default Palindrome;
